# Beholder

### Stack de ELk para el monitoreo de aplicaciones

[![Elastic Stack version](https://img.shields.io/badge/ELK-5.6.1-blue.svg?style=flat)](https://github.com/deviantony/docker-elk/issues/165)

### Configuración inicial

**Usuario:** admin
**Password:** welcometohell

Para configurar la contraseña hay que volver a crear el archivo `nginx/kibana.htpasswd`. 
Aún no logro crear el archivo del password desde Docker, espero corregirlo pronto. Hasta entonces, hay que crear el archivo
de configuración desde la máquina host.

```
sudo apt-get update
sudo apt-get install apache2-utils

htpasswd -c nginx/kibana.htpasswd admin
```

Una vez creado, basta con inicializar la aplicación corriendo `docker-compose up`.

### Configuración de puertos

Servicio | Puerto
---|---
Kibana | 5066
Elasticsearch | 8080
Logstash | 5000/tcp, 5044/tcp
